﻿using TUGraz.VectoCommon.Utils;

namespace TUGraz.VectoCommon.BusAuxiliaries {
	public interface IVehicleData
	{
		Kilogram TotalVehicleMass { get; }
	}
}